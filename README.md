[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# **ShinyBIOMOD**
<img src="inst/ShinyApp/www/ShinyBiomodTeam.png" alt="alt text" width="75%" height="75%">

<br><br>
Welcome to ShinyBIOMOD , a platform for Ensemble Modelling of Species Distributions , that helps using a range of statistical tools to examine the relationship between species and their environment, project their distribution in space and time, assess prediction uncertainty.


- **Screencast for the Ecological Niche Modeling 2020 course** ([ENM2020](https://www.youtube.com/watch?v=vj8qTo56rPA "ENM2020"))**:**
[![alt text](https://img.youtube.com/vi/nhWPYeLhxoA/3.jpg)](https://www.youtube.com/watch?v=nhWPYeLhxoA&feature=youtu.be "alt text")

- **MDPI Diversity Journal Student Poster Prize winner** at the [Species On the Move 2019](http://www.speciesonthemove.com/) International Conference Series ([see poster](SOTM2019_poster.png)) 

# ***What is ShinyBIOMOD ?***

ShinyBIOMOD is an R application providing a user-friendly interface and new functionalities to **BIOMOD (BIOdiversity MODelling,** [biomod2](https://cran.r-project.org/web/packages/biomod2/biomod2.pdf) package), through the [shiny](https://cran.r-project.org/web/packages/shiny/shiny.pdf) package. 
It aims to facilitate the creation of Species Distribution Models (SDMs) by guiding the user step-by-step through the modelling process. The objective is to provide an easy tool for users with or without prior programming knowledge and improve visualization of data, while ensuring the user to get familiar with good practices in Species Distribution Modelling. 
It combines:

- <mark style="background-color: lightblue"> the ability of biomod2 to model species distributions with an ensemble of statistical tools</mark> and project them into different scenarios of past, present and/or future environmental conditions (e.g. climate, land cover...etc)

- <mark style="background-color: lightblue"> an easy-to-use interface implemented with shiny</mark> which allows to explore, test and visualize more easily a wide range of modeling approaches and spatial data.


ShinyBIOMOD is based on the R package biomod2, the last version of BIOMOD which uses a larger set of statistical models (including the popular MaxENT), evaluation metrics and more possibilities for manipulating background/pseudo-absence data. 
In addition to the functions from biomod2, ShinyBIOMOD also comprises new tools to ***(i) account for sampling biases*** in observation data, ***(ii) select environmental predictors*** mostly by minimizing (multi-)collinearity , and ***(iii) visualize inputs/outputs*** and ***export*** results.

Learn more about BIOMOD:
- [Concept, structure and philosophy](https://www.youtube.com/watch?v=-IAdf8Vh6uY&feature=youtu.be "Concept, structure and philosophy") 
- see also [Single species modelling](https://www.youtube.com/watch?v=QrwqhJgRbnY&feature=youtu.be "Single species modelling"), [Multi species modelling](https://www.youtube.com/watch?v=E2I5PSdwrG8&feature=youtu.be "Multi species modelling") and [Specifics](https://www.youtube.com/watch?v=hEjhdURRy3o&feature=youtu.be "Specifics")

## *Installation*
Beforehand, make sure to have [*R*](https://cloud.r-project.org/ "R") or [*RStudio*](https://rstudio.com/products/rstudio/download/ "Rstudio") installed on your machine.

Some R packages need to be compiled from source, so if you are on Windows, you need to install [*Rtools*](http://cran.r-project.org/bin/windows/Rtools/) too.

Install *ShinyBIOMOD* and run the application with the following instructions.<br>
If the package `devtools` is not already installed run `install.packages("devtools")` in your console, then:
```R
devtools::install_gitlab("IanOndo/ShinyBIOMOD")
library(ShinyBIOMOD)
run_shinyBIOMOD()
```

# ***How does it work ?***

## *the workflow*
The scheme below represents the **general workflow** of ShinyBIOMOD. It describes how the user is guided throughout a series of mandatory and non-mandatory steps to build a **Species Distribution Model (SDM)** with the possibility of exploring and visualizing data at every of these steps. The workflow is designed to help the user to optimize the modelling process for best results and high efficiency. 
ShinyBIOMOD is an interactive tool that considers the data available to the user at each step before proceeding to the next ones. Indeed, new tabs and options appear dynamically according to the set of information entered by the user as he advances along the modelling process.

<img src="inst/ShinyApp/www/ShinyBiomod_workflow_chartv2.png" alt="alt text" width="75%" height="75%"> 

## *step-by-step*
## 0. Home page & working directory
Here is the homepage of *ShinyBIOMOD*. It essentialy welcomes the user with a brief about the app, the developement team and the research work r(references) the app is relying on.
By default the working directory is set to the "*HOME*" directory of your R(studio) session, if you want to change it, please navigate to the tab `Settings` and change the directory using the folder explorer in the `Set working directory` sub tab.

<br>

![](screenshots/screenshot_homepage_tab.png)

<br>

## 1. Data upload
Go to the `Data upload` tab and upload your occurrence and your environmental data. Occurrence data can be locally uploaded using a file explorer tool accessible in the `"Occurrence data/Upload your data"` sub tab. Environmental data can either be uploaded from:

-  A local disc, by using a file explorer tool in the sub tab `Environmental data/User-defined`

-  WorldClim database, by downloading the data via an “api” located in the sub tab `Environmental data/WorldClim`.

### 1.1  User occurrence data
In the first screenshot below, the red frame highlights the left-hand side panel from where you can import your occurrence data. The black arrow indicates the file explorer widget. Once your file is uploaded, you will be prompted to select which columns in your dataset refer to the *longitude*, *latitude* and *species* name. Then, you will need to select which species you want to work on, and validate your choice.
Once your parameters are set, the  main panel will display an interactive map in the tab `Map` as illustrated in the screenshot. Tables and statistics are displayed in the another tab `Tables` (the one pointed out by the upper black arrow).

<br>

![](screenshots/screenshot_user_occurrence_upload_tab2.png)

<br>

### 1.2  User-defined environmnental data
On this screenshot you can see the display of one environmental raster layer uploaded from a user local disc. This page has a security check to make sure the directory selected contains raster layers (a warning is displayed otherwise). The black arrow points towards a dropdown menu button allowing the user to:

-  select how many variables to display,

-  pick up a color palette

Also, there is a note just under the folder explorer that remembers the user that all the rasters must have the same extent and the same resolution, otherwise the app will send an error message.

<br>

![](screenshots/screenshot_user_raster_layers_tab2.png)

<br>

## 2. Data processing
You may have noticed that a new tab called `Data processing` popped up since you successfully uploaded and validated your data. 
This is for encouraging you to pre-process your occurrence dataset before moving forward. You can:

-  spatially filter your records in the `Spatial thinning/Thin your occurrence data`tab (if you suspect sampling issues).

-  constrain the geographic extent of your study area in the `Geographic constraints/Constrain your training area` tab to account for limiting dispersal capacity of your species for example.

<mark>The first screenshot is from the *Spatial thinning* tool, and describes the standard page layout displayed by most modules in *ShinyBIOMOD*:</mark>
- <mark>A brief description of features of the module (red frame)</mark>
- <mark>A set of parameters to fulfill before executing the routine (press button) (blue frame)</mark>
- <mark>The (map) output of the results followed by a short summary with certain information highlighted in colors to draw attention to these parameters. (green frame).</mark>


Both modules *Spatial thinning* and *Constraint of training area* allow the user to save their results for later use in the modelling process.

### 2.1 Spatial thinning
In the screenshot below, the thinning (filtering) parameter has been set to keep a minimum distance of 10 km between each records. The field "Number of replications" indicates that the filtering process for this particular example is replicated only once. The summary on the main panel indicates that the algorithm retained 120 records (out of > 500). Any records retained are mapped in green, and records removed in red.

<br>

![](screenshots/screenshot_thinning_tab2bis.png)

<br>

### 2.2 Constraint of the training area
In the example below the geographical extent selected for model training is constrained (limited) by a 100 km *circular buffer* size around the records. 
The resulting background area is then clipped to the coastline (to avoid generating pseudo-absences in non terestrial lands).
Many other options to define the training area are available:

- Manual drawing of the training area (not implemented yet !)
- Using a pre-defined region (e.g. expert opinion map or the estimated dispersal range of your species)
- Using similarity in environmnental conditions (environmnetal profiling, not implemented yet)
- Using a bounding box or a convex hull

<br>

![](screenshots/screenshot_constrained_area_tab.png)

<br>

## 3. Data exploration
As for occurrence data, you may have noticed that a new tab called `Data exploration` popped up since you uploaded your environmental data. It encourages you to explore the correlations among your environmental variables, and thus to remove the one causing collinearity issues. 
When you click to this tab,  you will find multiple sub tabs allowing you to perform:

- PCA
- Sparse PCA
- Correlation analysis

### 3.1 Principal Component Analysis
The *Principal Component Analysis* (PCA) is particularly useful for detecting which variables best describe the variability in environmental conditions of your dataset. On the left-hand side, select which transformation you want to apply to your dataset before applying the PCA and in how many dimensions you want to reduce its environmental space (number of axis). Then click on "Run PCA". The analysis produces 4 types of diagnostics:

- A barplot of the contribution of each axis (i.e. principal component) to the variability found in your data
- A barplot of the contribution of each variable to each selected principal components
- A correlation circle centred on the PCA projection plan
- A raster of the (relative and/or absolute) contribution of each grid cell to the PCA projection plan (i.e. how well each grid cell is represented on this plan) 

The app automatically displays the first 3 ones in the tabs `barplot` and `Correlation circle`. the third one must be selected from the options menu on the left-hand side. 

Variables ranked first in the first axis logically are the most relevant here.

<br>

![](screenshots/screenshot_PCA_barplot_tab.png)

<br>

On this screenshot, the horizontal axis is the first principal component, and the vertical one is the second principal component. 
Variables whose arrows are closer to a particular component, contribute more to this component. The length of an arrow represents the correlation of the variable to an axis (i.e. the longer it is, the greater is it is correlated to an axis). For example, *GLOBAL_SLOPE_10MIN* correlates more with the first principal component but its correlation is much lower than the ones from other variables.

<br>

![](screenshots/screenshot_PCA_corcircle_tab.png)

<br>

### 3.2 Correlation analysis
Moving to the `Correlation analysis` tab, this screenshot shows the result of a correlation analysis between a set variables. 50000 grid cells values from your rasters have been sampled, setting up a limit pairwise correlation to 0.5 (pretty gentle). The output showed in the `Results` tab listed only two pairs of variables with correlations above this threshold. 
The highest correlation found is only -0.559, considering collinerity issues arise with correlations usually > 0.7, one can consider there is no evidence of collinearity among the variables. But there multicollinearities (i.e. high correlations between more than 2 variables) may remain.
In this latter case, you can check other correlation methods:
- *VIF cor* 
- *VIF stepwise*

Both methods use the *Variance Inflation Factor* to exclude multi-collinear variables (i.e. variables correlated to many other variables).
You can have a look at the correlation matrix (pairwise correlations among all variables) in the tab `Table(s)`

<br>

![](screenshots/screenshot_Correlations_tab.png)

<br>

### 3.3 Model selection
There is a fourth tab called `Best subsets` which remains hidden if your occurrence dataset does not contain absences. Indeed, this tab is disabled as long as the app does not detect you have uploaded a presence-absence dataset. Otherwise, you can enter this tab and run a different model selection which consists in selecting a subset of environmental variables based on various criteria such as *information criteria* (AIC, BIC) and *cross-validation* (k-fold CV, leave-one-out).


## 4. Data preparation
Once you are satisfied with your occurrence dataset and environmental layers, go to the `Data preparation` tab, select the data you want from the left-hand side panel, and visualize them in the main panel on the right-hand side. Save and continue.

Selection fields on the left panel will propose to use *pre-processed* or *non pre-pocessed data* for model training, depending if you have saved your data as explained earlier (which is the case here). 
An interesting feature is that the app will detect if you have performed some correlation analyses (VIF cor or VIF step) and will propose to look at the subset (if any) of variables recommended from those analyses.

On the screenshot below, you can see that both occurrence data and environmnetal layers can be displayed on the same interactive map. 
Optionnaly, you can also download a dataframe with environmental values extracted at your occurrence locations. 

<br>

![](screenshots/screenshot_data_preparation_tab.png)

<br>

## 5. Ecological Niche Modelling
This is the core of the modelling process, where you can define a *model-based strategy to account for sampling bias* in your training records, *select one or more model algorithm(s)*, choose the *functional form of the response of your species to environmental gradients*...etc.

In this section (`ENM`), each sub tab correspond to one of the main features of the **biomod2** package: 
- Formatting
- Modelling
- Ensemble-Modelling

### 5.1 Formatting
In formatting, if you do not have true absence locations (or if you have too) but only presence records, you can generate *pseudo-absences* (PA) locations in order to contrast ecological conditions on those sites compared to the ones at your presence points. So select:
- A number of PA datasets
- A number of PA locations
- A strategy for generating those PA locations i.e. how would you like them to be sampled ? randomly across your background or according to a specific scheme ?

*ShinyBIOMOD* facilitates user-defined PA strategies by implementing 2 PA selection schemes used in the litterature to tackle sampling bias: 
- TargetGroup background selection scheme (TAG)
- Background thickening selection scheme (BT)

Both scheme sample PA locations from an estimate of sampling probablity based on the kernel density estimation of occurrence records. *TAG* uses occurrence records from a particular group of species including your species (often an higher taxonomic rank), and *BT* uses the training presence of your own species.

You just need to:
1.  upload your occurrence records as before, making sure to respect the format required.
2.  select a bandwidth for the kernel density estimation
3.  generate a sampling probability surface
4.  sample your PA locations
5.  save your PA dataset(s)

All those steps are included in the dropdown menu `Pseudo-absences selection` located on the left-hand panel as you can see on the screenshot below.
Only *user-defined* strategies are displayed on the main panel. Black dots represent the training presences, green dots the PA locations.

Once you are ready, click on the button `Run Formatting`. Wait while the app is computing and check the results. 

As already mentionned before, important information are highlighted with colors.
<br>

![](screenshots/screenshot_biomod_formatting_tab.png)

<br>

If the formatting of your data is done with success, it triggers the display of the next tab : `Modelling`.

### 5.2 Modelling
In `Modelling` you can choose (non exhaustive list):
- between 10 model algorithms to run
- several model tuning options including e.g. the form(ula) of the relationship between species occurrence and your environmental variables for regression-like models (in the red dropdown menu button under the selection field of model algorithm(s))
- a set of performance metrics to evaluate your model(s)
- to compute variable importance (i.e. how much the environmental suitability of your species changes when the variable is dropped out of the predictors)
- ...

A particularly interesting feature of *ShinyBIOMOD* is that you can save those parameters by clicking on a `save` button at the bottom end of the left-hand side panel. When you come back to this section, the app will ask you if you want to load previously saved modelling parameters. Answer "YES" if you want to, and the fields will be automatically populated with previous parameters.

Click on `Run BIOMOD Modeling` to fit your model(s). Check out the usual output.

<br>

![](screenshots/screenshot_biomod_modelling_tab.png)

<br>

### 5.3 Ensemble-Modelling
An `Ensemble-Modelling` tab appears. There you can choose which model(s) you would like to combine together, and how (which assembly rule) you want to combine them.

Then select an algorithm to generate predictions from multiple models, i.e. how would you like the predictions of each model to be combined ? 
- mean prediction ? 
- weighted mean ?
- ...

You can also choose to generate uncertainty values such as the coefficient of variation of the predictions.

The ouput from this module is similar to the `Modelling` one so, it is okay to skip it and navigate to the `Evaluations` tab.

## 6. Evaluations
Here you can check:

-  how well or bad your models have performed in predicting the presence of your species <i class='fa fa-check'></i>
-  which variables seems to be important for predicting your species occurrence
-  the plausibility of the response shape of your species to the variables used to model its distribution

### 6.1 Performance metrics
In the `Performance metrics` tab, on the left-hand panel, select which type of models you want to check (individual models or ensemble-models ?), which performance metric, PA dataset...etc.

Click on the `check` button to display the results.
A barplot display the variable importance score in the lower right corner of the panel. Scores are ordered from the highest to the lowest for better visualization.
On the right, a data table shows evaluation metric scores selected, with additional information such as the threshold value associated with the confusion matrix used to calculate those metrics.
<br>

![](screenshots/screenshot_perf_metric_and_variable_importance_tab.png)

<br>

### 6.2 Response function
In the `Response function Uni/Bivariate` sub tab, you can inspect how your species responds to environmental variables.
The response is scale between 0 [low probability of presence] and 1 [high probability].
Same as for the `Performances metrics`, you can specify whcih model you want the response shapes from on the left-hand side panel.
Univariate response curves can be traced for each variable, while the others are held at a constant metric value to be selected among: 
the *mean* (default), the *median*, the *minimum* or the *maximum*.
<br>

![](screenshots/screenshot_response_curves_tab.png)

<br>

## 7. Projections
This is the last tab that will be triggered if you have successfully run an Ensemble-Model. At the moment only the `Ensemble Forecasting` tab is enabled since you have not generated your ensemble-model projections yet.

In this tab, you need to select (non exhaustive list):
-  the geographic extent in which projecting your model(s)
- A set of environmental variables: <mark> make sure they have the same names as the ones you used for model training. </mark>

These variables can be past or future projections of the ones you used for training.
- which ensemble-model(s) you want to project.

Finally, click on `Run BIOMOD EnsembleForecasting`. Check the output.
I will spare you the screenshot of this output since it is similar to the other modelling outputs.

Once you have generated the projections of your ensemble-model(s), 3 new tabs appear:
- `Thresholding` (Binary maps)
- `project as a raster` and `project onto geography` (Project maps)

### 7.1 Thresholding
The `Thresholding` tab will help you to convert your probability map(s) generated by the ensemble forecasting into binary maps (i.e.presence/absence map).

Several criteria are available to do this conversion. You can :
- manually select a threshold i.e. selecting a probability range beyond which you consider your species absent from an area
- select one or more criteria that will calculate optimized thresholds for this conversion.

Then, click on `Convert your map`. 

As shown in the screenshot below, the output produces as many maps as criteria selected (see black arrows), and a tab summarizing the methods (criteria) used, the optimized threshold(s) computed, the entries on the confusion matrix computed and an estimation of the occupied area predicted by this range map. The green circle on the lower right corner shows a legend to better understand the confusion matrix entries.

As you can see, different methods/criteria lead to different predicted distribution area, thus different range size estimations.
<br>

![](screenshots/screenshot_binary_conversion_tab.png)

<br>

There are options to save those binary maps, for visualization with the interactive tool in the `Project onto geography` tab for example.

### 7.2 Projection maps
Finally in the following tabs you will find the map of your projections as a raster (first screenshot), and an interactive map (second screenshot), maybe more interesting since you can zoom in/out, drag the map to some particular areas, and add your occurrence records.
<br>

![](screenshots/screenshot_project_raster_tab.png)

<br>

<br>

![](screenshots/screenshot_project_leaflet_raster.png)

<br>


## *the folder structure*
The workflow described previously automatically generates a project with folders and files associated to the user activity during his session. 
The project is a single root directory named `SHBMD` (abbr. of ShinyBIOMOD) and is hierarchically structured, starting with a limited number of general folders, then broadens towards more specific folders including those generated by the `biomod2` package.

By saving the user workflow in a self-contained organised folder system struture, *ShinyBIOMOD* ensures **reproducibility** of your research, **provenance** of your inputs and **portability** of your results (e.g. when moving from computer to computer).
A detailled scheme of the folder structure will be available soon from the home page of the app.

Here is a quick example of how the app responds when the user enters a (working) directory containing a *ShinyBIOMOD* project.
It simply informs the user that the output of a model previously run is available for display (example taken from the `Modelling...` sub tab).

<br>

![](screenshots/screenshot_click_here.png)

<br>



## ***Known issues***

### *reactivity*
*Reactivity* is what makes the app responsive to the user inputs. It updates the app whenever the user makes changes. In *ShinyBIOMOD* some expressions are very complex, with a deep graph dependency, so re-running your analysis in a very short amount of time may cause the app to become unresponsive, bogging down or even crash. I'm working on improving the reactivity of certain sections to avoid that and enhance user experience. 
Meanwhile, if for some reasons the app does not respond as expected e.g. tabs that do not show up, outputs that do not display...etc, please try the following options:
1. ...Wait...
2. Re-run the analysis (re-click the button)
3. Navigate to another tab, then come back to your current one
4. Close and re-start the app (model objects generated are automatically saved and will be available when accessing your working directory)
5. Check out the log files generated by the biomod2 modelling sections to see if any error has been reported. You can find this file here: 
`<your working directory> --> SHBMD --> SavedObj --> MyBioModData --> (Formating|Modeling) --> <your species name> --> <species.name>_<biomod_procedure>Out.log`

### *multispecies*
Multispecies modelling is still experimental and works fine for simple workflows. However, when modelling multiple species with a different set of parameters, (hundreds of) R expressions need independent evaluations for each species which creates a very complex reactivity structure.
A recommendation to avoid problems when modelling multiple species:
- Run your analyses one species at a time *(this is the default in the modelling section)*
<br>
<mark>*ShinyBIOMOD* is not designed to model say, more than half a dozen species at the time</mark>.Visualization tools and computations may not respond well. The app works best with one or two species.

# ***References***

Thuiller, W., Lafourcade, B., Engler, R. and Araújo, M. B. 2009. BIOMOD - a platform for ensemble forecasting of species distributions. - Ecography 32: 369 - 373. [DOI:10.1111/j.1600-0587.2008.05742.x](https://doi.org/10.1111/j.1600-0587.2008.05742.x)

Thuiller, W., Georges, D., Engler, R. & Breiner, F. Package "biomod2"; (2016)