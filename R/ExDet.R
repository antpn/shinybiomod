# Functions developed by DRS for Bell DM, Schlaepfer DR (2016) On the dangers of model complexity without ecological justification in species distribution modeling. Ecological Modelling, 330, 50-59.
# see https://github.com/bellland/SDM.Virtual.Species_Bell.Schlaepfer

#' Univariate extrapolation
#'
#' @param refdat a data.frame with entries representing reference values
#' @param refdat a data.frame with entries representing projection values
#' @seealso https://github.com/bellland/SDM.Virtual.Species_Bell.Schlaepfer
#' @return a vector whose negative values (<0) indicate entries in the projection data whose values are out of the range of the reference data;
#'                        values equal to 0 indicate entries within the range of the reference data
#' @export
calc_NT1 <- function(refdat, prodat) {
	stopifnot(identical(colnames(refdat), colnames(prodat)))

	#——————————————————————–#
	# NT1 – UNIVARIATE EXTRAPOLATION
	#——————————————————————–#
	# Mesgaran, M. B., R. D. Cousens, B. L. Webber, and J. Franklin. 2014. Here be dragons: a tool for quantifying novelty due to covariate range and correlation change when projecting species distribution models. Diversity and Distributions 20:1147-1159.
	# code based on comment by Matthew Bayly to https://pvanb.wordpress.com/2014/05/13/a-new-method-and-tool-exdet-to-evaluate-novelty-environmental-conditions/
	require(matrixStats)
	range_ref <- t(matrixStats::colRanges(refdat,na.rm=TRUE))#t(apply(refdat,2,range,na.rm=TRUE))#
	#dimnames(range_ref) <- list(c("min", "max"), colnames(refdat))
	range_ref_arr <- array(range_ref, dim = c(dim(range_ref), nrow(prodat)), dimnames = list(c("min", "max"), colnames(refdat), NULL))
	diffs_ref <- matrixStats::colDiffs(range_ref)#apply(range_ref,2,diff,na.rm=TRUE)#
	#colnames(diffs_ref) <- colnames(refdat)
	diffs_ref_arr <- matrix(diffs_ref, nrow = nrow(prodat), ncol = ncol(prodat), byrow = TRUE)

	if(ncol(prodat)>1){
		iud <- array(0, dim = c(dim(prodat), 3))
		iud[, , 2] <- prodat - t(range_ref_arr["min", ,])
		iud[, , 3] <- t(range_ref_arr["max", ,]) - prodat
	}
	else{
		iud <- array(0, dim = c(dim(prodat), 3))
		iud[, , 2] <- as.vector(prodat) - t(range_ref_arr["min", ,])
		iud[, , 3] <- t(range_ref_arr["max", ,]) - as.vector(prodat)
	}

	UDs <- apply(iud, 1:2, min, na.rm=TRUE) / diffs_ref_arr
	NT1 <- rowSums(UDs)

	return(NT1)
}

#' @export
calc_MIC_NT1 <- function(refdat, prodat) {
  stopifnot(identical(colnames(refdat), colnames(prodat)))

  #——————————————————————–#
  # NT1 – UNIVARIATE EXTRAPOLATION
  #——————————————————————–#
  # Mesgaran, M. B., R. D. Cousens, B. L. Webber, and J. Franklin. 2014. Here be dragons: a tool for quantifying novelty due to covariate range and correlation change when projecting species distribution models. Diversity and Distributions 20:1147-1159.
  # code based on comment by Matthew Bayly to https://pvanb.wordpress.com/2014/05/13/a-new-method-and-tool-exdet-to-evaluate-novelty-environmental-conditions/
  require(matrixStats)
  range_ref <- t(matrixStats::colRanges(refdat,na.rm=TRUE))#t(apply(refdat,2,range,na.rm=TRUE))#
  #dimnames(range_ref) <- list(c("min", "max"), colnames(refdat))
  range_ref_arr <- array(range_ref, dim = c(dim(range_ref), nrow(prodat)), dimnames = list(c("min", "max"), colnames(refdat), NULL))
  diffs_ref <- matrixStats::colDiffs(range_ref)#apply(range_ref,2,diff,na.rm=TRUE)#
  #colnames(diffs_ref) <- colnames(refdat)
  diffs_ref_arr <- matrix(diffs_ref, nrow = nrow(prodat), ncol = ncol(prodat), byrow = TRUE)

  if(ncol(prodat)>1){
    iud <- array(0, dim = c(dim(prodat), 3))
    iud[, , 2] <- prodat - t(range_ref_arr["min", ,])
    iud[, , 3] <- t(range_ref_arr["max", ,]) - prodat
  }
  else{
    iud <- array(0, dim = c(dim(prodat), 3))
    iud[, , 2] <- as.vector(prodat) - t(range_ref_arr["min", ,])
    iud[, , 3] <- t(range_ref_arr["max", ,]) - as.vector(prodat)
  }

  UDs <- apply(iud, 1:2, min, na.rm=TRUE) / diffs_ref_arr
  UDs[UDs==0] <- NA
  MIC <- min.col(UDs)

  return(MIC)
}

#' Multivariate extrapolation
#'
#' @param refdat a data.frame with entries representing reference values
#' @param refdat a data.frame with entries representing projection values
#' @seealso https://github.com/bellland/SDM.Virtual.Species_Bell.Schlaepfer
#' @return a vector of values
#' @export
calc_NT2 <- function(refdat, prodat){
	stopifnot(identical(colnames(refdat), colnames(prodat)))

	#——————————————————————–#
	# NT2 – MULTIVARIATE EXTRAPOLATION
	#——————————————————————–#
	# Mesgaran, M. B., R. D. Cousens, B. L. Webber, and J. Franklin. 2014. Here be dragons: a tool for quantifying novelty due to covariate range and correlation change when projecting species distribution models. Diversity and Distributions 20:1147-1159.
	# code modified from on https://pvanb.wordpress.com/2014/05/13/a-new-method-and-tool-exdet-to-evaluate-novelty-environmental-conditions/

	# Calculate the average and covariance matrix of the variables
	# in the reference set
	ref.av  <- colMeans(refdat, na.rm = TRUE)#apply(refdat,2,mean,na.rm=TRUE)#
	ref.cov <- var(refdat, na.rm = TRUE)

	# Calculate the mahalanobis distance of each raster
	# cell to the environmental center of the reference
	# set for both the reference and the projection data
	# set and calculate the ratio between the two.
	mah.ref <- mahalanobis(x = refdat, center = ref.av, cov = ref.cov)
	mah.pro <- mahalanobis(x = prodat, center = ref.av, cov = ref.cov)
	mah.max <- max(mah.ref[is.finite(mah.ref)|!is.na(mah.ref)])
	NT2 <- mah.pro / mah.max

	return(NT2)
}

#' @export
calc_MIC_NT2 <- function(refdat, prodat){
  stopifnot(identical(colnames(refdat), colnames(prodat)))

  #——————————————————————–#
  # NT2 – MULTIVARIATE EXTRAPOLATION
  #——————————————————————–#
  # Mesgaran, M. B., R. D. Cousens, B. L. Webber, and J. Franklin. 2014. Here be dragons: a tool for quantifying novelty due to covariate range and correlation change when projecting species distribution models. Diversity and Distributions 20:1147-1159.
  # code modified from on https://pvanb.wordpress.com/2014/05/13/a-new-method-and-tool-exdet-to-evaluate-novelty-environmental-conditions/

  # Calculate the average and covariance matrix of the variables
  # in the reference set
  ref.av  <- colMeans(refdat, na.rm = TRUE)#apply(refdat,2,mean,na.rm=TRUE)#
  ref.cov <- var(refdat, na.rm = TRUE)

  # Calculate the mahalanobis distance of each raster
  # cell to the environmental center of the reference
  # set for both the reference and the projection data
  # set and calculate the ratio between the two.
  mah.pro.all <- mahalanobis(x = prodat, center = ref.av, cov = ref.cov)
  IC <- array(dim=c(nrow(prodat),ncol(prodat)))
  for(j in 1:ncol(prodat)){
    mah.pro <- mahalanobis(x = prodat[,-j], center = ref.av[-j], cov = ref.cov[-j,-j])
    IC[,j] <- 100* ((mah.pro.all - mah.pro)/mah.pro.all)
  }
  MIC <- max.col(IC)

  return(MIC)
}

#' Multivariate extrapolation
#'
#' @param refdat a data.frame with entries representing reference values
#' @param refdat a data.frame with entries representing projection values
#' @seealso https://github.com/bellland/SDM.Virtual.Species_Bell.Schlaepfer
#' @return a vector whose negative values (<0) indicate entries in the projection data whose values are out of the range of the reference data;
#'                        values between 0 and 1 (0< value <1), indicate entries within the range of the reference data entries and whose values close to 1 are closer to the values of the reference data;
#'                        values superior to 1 (>1), indicate entries within the range of the reference data but whose correlations between variables are out of the range of those in the reference data.
#' @export
exdet <- function(refdat, prodat) {
	stopifnot(identical(colnames(refdat), colnames(prodat)))

	# test for novelty of type 1
	NT1=calc_NT1(refdat, prodat)

	# test for novelty of type 2 on remaining points (i.e. inside the range where NT1=0)
	stopifnot(any(NT1==0))

	newprodat = prodat[NT1==0,,drop=FALSE]

	NT2 = calc_NT2(refdat, newprodat)

	exdet_dat = replace(NT1, NT1==0, NT2)

	return(exdet_dat)
}

#' utility function: find the minimum position for each row of a matrix, breaking ties at random.
#' @export
min.col <- function(m, ...) max.col(-m,...)
