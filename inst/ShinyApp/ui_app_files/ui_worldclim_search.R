gcmchoices <- list('Select representative concentration pathways'=c(1))
# GUI for searching WORLDCLIM data

wc_layers <- sidebarLayout(position = "left",

					sidebarPanel(

						tags$head(
						
						  tags$script(src = 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))
						
						),
						
						titlePanel("EcologicalNicheToolBox"),
						
						h2("WorldClim data"),
						
						#p("Here you can request data to the", a(href="http://www.gbif.org/",target='_blank',"GBIF"),
						
						#   "API using the",  a(href="http://cran.r-project.org/web/packages/spocc/vignettes/spocc_vignette.html",
						
						#                       target='_blank',"spocc"),"package"),
						
						HTML("<p align ='justify'>Here you can request data from the </p>"),
						
						a(href="http://www.worldclim.org/",target='_blank',"WORLDCLIM"),
						
						HTML("database using the"),
						
						a(href="https://cran.r-project.org/web/packages/raster/vignettes/Raster.pdf",target='_blank',"raster"),
						
						HTML("package</p>"),
						
						p("Not all combinations are available. See the worldclim link above for details"),
						
						br(),
						
						wellPanel(
						
							h3("Climate conditions"),	
						
							radioButtons(inputId='WC_cond',
								
								label='Select one climate condition',
							
								choices=c("Current"="current_cond","Futur"="futur_cond"),
							
								selected="current_cond"
							),

							conditionalPanel(condition="input.WC_cond=='futur_cond'",
							
								h3("Time period"),	
								
								tags$div(title="",
								
									radioButtons(inputId='WC_year',
									
										label='Select one time period', 
									
										choices=c('2050 (average for 2014-2060)'=50,'2070 (average for 2061-2080)'=70),
									
										selected=70
									)
								
								),
								
								h3("Representative concentration pathways"),

								checkboxGroupInput(inputId='WC_rcp',

									label='Select one (or more) representative concentration pathways',
								
									choices=list('rcp 26'=26,'rcp 45'=45, 'rcp 60'=60,'rcp 85'=85),
								
									selected=c(45,85)
								),
								
								h3("Global Circulation Models"),
								
								selectInput(inputId='WC_gcm',

									label='Select one (or more) global circulation models',
							
									choices=names(gcmchoices)[1],
										
									multiple=TRUE,
										
									selected=c('CN')
								)						
							),
								
							h3("Variable"),
						
							radioButtons(inputId='WC_var',

								label='Select one variable',
							
								choices=c('Monthly average min temperature'='tmin','Monthly average max temperature'='tmax','Monthly total precipitation'='prec', 'Bioclimatic'='bio'),
							
								selected = 'bio'
							),
								
							h3("Grid resolution"),
						
							radioButtons(inputId='WC_resol',

								label='Select one grid resolution',
							
								choices=c('10 Arc-minutes'='tenArcs','5 Arc-minutes'='fiveArcs','2.5 Arc-minutes'='twoArcs', '30 Arc-seconds'='zerofiveArcs'),
							
								selected = 'tenArcs'
							
							)							
						
						),	
						
						br(),
						
						bsButton(input="load_worldclim_layers",label="Load WorldClim rasters",icon = icon("upload", lib = "glyphicon"), styleclass="primary"),
						
						busyIndicator("Searching...",wait = 0),
						
						br(),
						
						br()#,
						
						# conditionalPanel(
						
							# condition="inputId.load_worldclim_layers > 0",
							
						    # radioButtons(inputId='WC_output_format',
							
								# label='Format',
							
								# choices= c('.GRD' = "raster", '.ASC' = "ascii", '.BIL' = "BIL", '.SDAT' = "SAGA", '.RST' = "RST", '.TIF' = "GTiff", '.ENVI' = "ENVI",'.IMG' = "HFA"),

								# selected="ascii"
							# ),
							
							# br(),
							
							# downloadButton(outputId="downloadWorldClim",label="Download rasters layers")
						# )
					),
						
					mainPanel(
					
						plotOutput(outputId="wc_explvar_layers"),					
					
						h4("Summary"),
						
						verbatimTextOutput(outputId="summary_rasters")
      
					)
		)
		
		
		