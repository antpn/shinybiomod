
ui_thresholding_methods <- fluidPage(

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

					titlePanel(h1(strong(em("Binary map conversion")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

					sidebarLayout(position = "left",

						sidebarPanel(

							tags$head(

								tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))

							),

							hr(),

							div(style="text-align: justify",
								p(strong("Convert"),"your",strong(em("probability map(s)"))," into a ", strong(em("Presence-Absence map")),"(i.e. a binary map delineating areas where your species is present and absent), using the",a(href="https://cran.r-project.org/web/packages/biomod2/biomod2.pdf",target='_blank',strong(em("PresenceAbscence"))),"package."),
								tags$ul(style="font-family: 'times';font-size: 18px;",
                        tags$li("Explore a range of",strong(em("criteria")),"calculated with", strong(em("optimized thresholds."))),
								        tags$li("Evaluate how the",strong(em("predicted occupied area")),"varies as the threshold and/or the criterion used changes.")
							  )
							),

							hr(),

							h3("Biomod settings"),

							uiOutput('chooserThresholdingSpecies'),

							selectizeInput(inputId='biomod_check_thresholds_modeling_procedure_Outputs',

								label = strong("Select the BioMod projection procedure you want to check :"),

								choices = NULL,

								options=list(placeholder="Select your BioMod projection procedure")
							),

							selectizeInput(inputId = "biomod_check_thresholds_projection",

								label=strong("Select a projection :"),

								choices=NULL,

								options=list(placeholder="Select a projection")
							),

							selectizeInput(inputId='biomod_check_thresholds_models_Outputs',

								label = strong("Select model(s) :"),

								choices = NULL,

								#multiple=TRUE,

								options=list(placeholder="Select your model(s)")
							),

							div(align='center', radioButtons(inputId='Have_eval_data_for_thresholding',label=shiny::span(icon('question-circle',lib="font-awesome"), strong('Have evaluation data ?'), style='font-size: 18px'),choices=c('YES', 'NO'), selected="NO", inline=TRUE)),

							hidden(

								div(id="biomod_thresholding_eval_data_panel",

									h3('Import evaluation data'),

									radioButtons(inputId='biomod_check_thresholds_eval_data_format',label=strong('Format'),choices=c('CSV', 'TXT'), selected="TXT"),

									fileInput(inputId='biomod_check_thresholds_eval_data',

										label=strong('Input evaluation data file'),

										accept=c("text/csv","text/comma-separated-values,text/plain",".csv","text/txt",".txt")
									),

									div(class="warning",p(style="color:#8d5524; padding-top: 0.5em; padding-bottom: 0.5em; padding-left: 0.5em; border: 1px; border-color:#8d5524;",icon("exclamation-triangle",lib="font-awesome"),em("Please make sure your dataset is formatted as shown in the example below"))),

									div(align='center',
									    tags$img(src='ShinyBiomod_data_format2.png', height='75%', width='80%', style="background-color: white;")
										# HTML(
										# htmlTable::htmlTable(data.frame(species=c("sp1","sp1","sp1",":","spX"),longitude=c(rep("...",3),':','...'),latitude=c(rep("...",3),':','...'),PresAbs=c("1","0","1",":","NA")), rnames=c("1","2","3",":","N"),css.cell="padding: 0 30px 0 30px;",
										# css.table= "font-style: italic; background-color: white; border-bottom: 2px solid #BEBEBE; border-left: 2px solid #BEBEBE; border-right: 2px solid #BEBEBE; font-weight: 900",pos.caption="bottom")
										# )
									),
									helpText(em("Note: the dataset must contain a column whose values indicate presences(1), true absences(0) or no information(NA). The names of the columns can be different than the ones shown in the example")),

									hidden(
										div(id="biomod_check_thresholds_eval_data_panel",
											uiOutput("biomod_check_thresholds_eval_data_select_inputs")
										)
									)
								)
							),

							hr(),

							h3("Conversion settings"),

							uiOutput('biomod_check_thresholds_conversions_settings'),

							br(),

							awesomeCheckbox(inputId="biomod_thresholding_only_active_tab", label=strong("run current species only"), value = TRUE, status = "primary"),

							tags$style(appCSS),

							div(align='center',
								withBusyIndicatorUI(bsButton(inputId="biomod_check_thresholds_check_thresholding", label = "Convert your map",	icon = icon("cut", lib = "font-awesome"), style = "primary"))
							),

							hr(),

							hidden(

								div(id="biomod_thresholding_save_options_panel",

									radioButtons(inputId='biomod_thresholds_save_criteria',

										label = strong('Select an output method:'),

										choices = c('Default','Sens=Spec','MaxSens+Spec','MaxKappa','MaxPCC','PredPrev=Obs','ObsPrev','MeanProb','MinROCdist','ReqSens','ReqSpec','MinTrainPres','Cost'),

										selected = 'MaxSens+Spec'

									),

									radioButtons(inputId='biomod_thresholds_save_output_format',

										label = strong('Select an output format:'),

										choices = c('GRD'='GRD','ASC'='ASC'),

										selected = 'GRD'
									),

									div(align='center',
										withBusyIndicatorUI(bsButton(inputId="biomod_thresholds_save_maps", label = "Save",	icon = icon("inbox", lib = "font-awesome"), style = "primary"))
									)

								)
							)
						),

						mainPanel(

							uiOutput("biomod_check_thresholds_outputs_panelSpecies")
						)
					)
				)
